import React, { Component} from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Todos from './component/Todos';
import Header from './component/layout/Header';
import AddTodo from './component/AddTodo';
import About from './component/page/About';
import './App.css';
import Axios from 'axios';

class App extends Component {
  state = {
    todos : []
  };
  componentDidMount() {
    Axios.get('https://jsonplaceholder.typicode.com/todos?_limit=10').then((res) => this.setState({todos : res.data}));
  }
  markComplete = (id) => {
    this.setState({ todos: this.state.todos.map(todo => {
      if (todo.id === id) {
          todo.completed = !todo.completed;
        }
        return todo;
      })
    });
  }
  delTodo = (id) => {
    this.setState({ todos: [...this.state.todos.filter(todo => todo.id !==id)]});
  }
  addTodo = (title) => {
    let newObj = [];
    newObj = this.state.todos;
    let newId = 0;
    if(newObj.length > 0) {
      newId = (newObj[newObj.length - 1].id) + 1;
    } else {
      newId = 1;
    }
    const newTodo = {
      id: newId,
      title,
      completed: false
    }
    this.setState({ todos: [...this.state.todos, newTodo]});
    console.log(this.state.todos);
  }
    render() {
      return (
        <Router>
          <div className="App">
            <Header />
            <Route exact path="/" render={props => (
              <React.Fragment>
                <Todos todos={this.state.todos} delTodo = {this.delTodo} markComplete={this.markComplete} />
                <AddTodo addTodo = {this.addTodo} />
              </React.Fragment>
            )} />
            <Route path="/about" component={About} />
          </div>
        </Router>
      );
    }
}

export default App;
