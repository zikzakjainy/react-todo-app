import React, { Component } from 'react'

export default class TodoStatus extends Component {
  render() {
    return (
      <i style={{color: (this.props.icon)? 'gray' : 'red'}}>{this.props.icon ? 'complete' : 'needToDo'}</i>
    )
  }
}
