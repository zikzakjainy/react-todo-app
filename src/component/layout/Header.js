import React, { Component } from 'react';
import {Link} from 'react-router-dom';
export default class Header extends Component {
  render() {
    return (
      <div className="header-text header-box">
        <h1>your Todos</h1>
        <Link to='/' className="link-text">Home</Link> |
        <Link to='/about' className="link-text">About</Link>
      </div>
    )
  }
}