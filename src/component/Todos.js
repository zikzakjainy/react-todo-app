import React, { Component} from 'react';
import TodoItem from './TodoItem';
import PropTypes from 'prop-types';
class Todos extends Component {
    render () {
        // let data = this.props.todos;
        const tasks = this.props.todos;
        return (
            <ul>
              {tasks.map((todo) => (
              <TodoItem  key={todo.id} todo={todo} markComplete={this.props.markComplete} delTodo={this.props.delTodo} />
              ) )}
            </ul>
          );
    }
}
// propType
Todos.propTypes = {
  todos: PropTypes.array.isRequired
}

export default Todos;