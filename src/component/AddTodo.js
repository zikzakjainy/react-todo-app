import React, { Component } from 'react'

export default class AddTodo extends Component {
  state = {
    title: ''
  }
  submit = (e) => {
    e.preventDefault();
    this.props.addTodo(this.state.title);
    this.setState({title : ''});
  }
  updateInput = (e) => this.setState({ title : e.target.value });
  /*
    in case of number of input fields so we can set input value like : 
    updateInput = (e) => this.setState({ [e.target.name] : e.target.value });
  */
  render() {
    return (
      <form onSubmit={this.submit} className="add-todo" style={{display: 'flex'}}>
        <input
          type="text"
          name="title"
          className="add-todo-input"
          style={{ flex: '10'}}
          placeholder="Add Todo..."
          value={this.state.title}
          onChange={this.updateInput} />
        <input 
          type="submit"
          value="submit"
          className="add-todo-btn"
          style={{flex: '1'}} />
      </form>
    )
  }
}

