import React, { Component } from 'react'
import PropTypes from 'prop-types';
import TodoStatus from './TodoIcon';
export class TodoItem extends Component {
  getStyle = () => {
    return {
      background: '#f4f4f4',
      padding: '10px',
      borderBottom: '1px dotted black',
      textDecoration : this.props.todo.completed ? 'line-through': 'none',
      boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)'
      }
  }
  
  render() {
    const {id, title, completed} = this.props.todo
    return (
      <div style={this.getStyle()}>
        <div style={pStyle}>
          <p style={{backgroundColor: '#f4f4f4'}}>
          <input type="checkbox" onChange={this.props.markComplete.bind(this, id)} /> {' '}
          {title} {'  '}
          <TodoStatus icon={completed} />
          {'                         '}
          <button style={btnStyle} onClick={this.props.delTodo.bind(this, id)}>
            X
          </button>
          </p>
        </div>
      </div>
    )
  }
}
const pStyle = {
  color: '#111'
}
const btnStyle = {
  background: '#ff0000',
  color: '#fff',
  border: 'none',
  padding: '5px 8px',
  borderRadius: '50%',
  cursor: 'pointer',
  alignItems: 'center'
}
TodoItem.propTypes = {
  todo: PropTypes.object.isRequired
}
export default TodoItem
